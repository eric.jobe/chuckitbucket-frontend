import React, { useState, useEffect, ReactNode } from 'react';
import axios from 'axios';
import FileDownloadIcon from '@mui/icons-material/FileDownload';
import '../styles/list.css';

const BucketList = () => {
    const [selectedFiles, setSelectedFiles] = useState([]);
    const handleDownload = (file: any) => {
        const downloadURL = 'https://t9a4ekpxnc.execute-api.us-east-1.amazonaws.com/chuckitbucket/chuckitbucket';
        const params = {
            key: file
        }
        axios.get(downloadURL, { 
            headers: {
                'Content-Type': 'application/json', 
            },
            params: params, 
        }).then((resp) => {
            console.log("File download successfully!");
            console.log(resp);
        });
    }

    const listObjects = selectedFiles.map(file => (
        <li key={file}>
            <div className='item'>
                <div className='download-button' onClick={() => handleDownload(file)}><FileDownloadIcon sx={{ display: { xs: 'none', md: 'flex' }, mr: 1 }} /></div>
                <span>{ file }</span>
            </div>
        </li>
    ));

    const baseURL = 'https://t9a4ekpxnc.execute-api.us-east-1.amazonaws.com/chuckitbucket/list';

    useEffect(() => {
        axios.get(baseURL, { 
            headers: {
                'Content-Type': 'application/json', 
            } 
        }).then((resp) => {
            const { objects } = resp.data;
            setSelectedFiles(objects);
            console.log("Files retrieved successfully!");
            console.log(selectedFiles);
        });
    }, []);

    return (
        <div>
            <div>
               
            </div>
            <div>
               <ul> 
                    { listObjects }
               </ul>
            </div>
        </div>
    );
};

export default BucketList;