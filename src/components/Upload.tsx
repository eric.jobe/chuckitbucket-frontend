import React, { useState } from 'react';
import axios from 'axios';

const UploadFile = () => {
    const [selectedFile, setSelectedFile] = useState<File | null>(null);

    const baseURL = 'https://9b8qj6s003.execute-api.us-east-1.amazonaws.com/chuckitbucket/'

    const handleFileChange = (event: any) => {
        setSelectedFile(event.target.files?.[0]);
    }

    const handleUpload = async () => {
        if (!selectedFile) {
            return;
        }

        const formData = new FormData();
        formData.append('file', selectedFile);

        try {
            await axios.post(baseURL, formData, {
                headers: {
                    'Content-Type': 'multipart/form-data'
                }
            });
            console.log('File uploaded successfully!');
        } catch (err) {
            console.error(err);
        }
    }

    return (
        <div>
            <div>

            </div>
            <div>
                <input type="file" onChange={handleFileChange} />
                <button onClick={handleUpload}>Upload</button>
            </div>
        </div>
    );
};

export default UploadFile;