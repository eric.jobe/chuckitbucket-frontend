import BucketList from './components/BucketList';
import TopNav from './components/TopNav';

import './styles/App.css';

function App() {
  console.log("App");
  return (
    <div className="App">
      <TopNav />
      <BucketList />
    </div>
  )
}

export default App
